<?php

namespace app\models\search;

use app\models\Aholi;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LocalityPlans;

/**
 * LocalityPlansSearch represents the model behind the search form of `app\models\LocalityPlans`.
 */
class LocalityPlansSearch extends LocalityPlans
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'locality_id', 'district_id', 'region_id'], 'integer'],
            [['year'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $subQuery = Aholi::find()->select('plan_id');
        $query = LocalityPlans::find();
        $query->where(['year'=>2019,'region_id'=>Yii::$app->access->getRegionId()]);
        $query->andWhere(['not in', 'id', $subQuery]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'locality_id' => $this->locality_id,
            'district_id' => $this->district_id,
            //'region_id' => $this->region_id,
        ]);

        //$query->andFilterWhere(['like', 'year', $this->year]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function filled($params=null)
    {
        $subQuery = Aholi::find()->select('plan_id');
        $query = LocalityPlans::find();
        $query->with('aholis');
        $query->where(['year'=>2019,'region_id'=>Yii::$app->access->getRegionId()]);
        $query->andWhere(['in', 'id', $subQuery]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'locality_id' => $this->locality_id,
            'district_id' => $this->district_id,
            //'region_id' => $this->region_id,
        ]);

        //$query->andFilterWhere(['like', 'year', $this->year]);

        return $dataProvider;
    }
}
