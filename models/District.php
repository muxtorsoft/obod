<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%district}}".
 *
 * @property int $id
 * @property int $region_id
 * @property string $title
 * @property string $type это для тип района и города чтобы отличить их из одного
 * @property int $order
 *
 * @property Region $region
 * @property Locality[] $localities
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%district}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_id', 'order'], 'integer'],
            [['title'], 'required'],
            [['type'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'title' => Yii::t('app', 'Title'),
            'type' => Yii::t('app', 'это для тип района и города чтобы отличить их из одного'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalities()
    {
        return $this->hasMany(Locality::className(), ['district_id' => 'id']);
    }

    /**
     * @param $name
     * @return int
     */
    public function idByNameRegId($name,$regId,$type='district')
    {
        $district = self::find()->where(['title' => $name,'region_id'=>$regId])->one();
        if ($district == null) {
            $district = new District();
            $district->title = $name;
            $district->region_id = $regId;
            $district->type = $type;
            $district->save();
        }
        return $district->id;
    }
}
