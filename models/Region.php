<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%region}}".
 *
 * @property int $id
 * @property string $title
 * @property int $order
 *
 * @property District[] $districts
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['order'], 'integer'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'order' => Yii::t('app', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['region_id' => 'id']);
    }

    /**
     * @param $name
     * @return int
     */
    public function idByName($name)
    {
        $region = self::find()->where(['title' => $name])->one();
        if ($region == null) {
            $region = new Region();
            $region->title = $name;
            $region->save();
        }
        return $region->id;
    }

    public function getEmployee()
    {
        $emplys = UserAccess::find()->where(['region_id'=>$this->id])->all();
        $users = [];
        if($emplys!=null){
            /** @var $u UserAccess */

            foreach ($emplys as $u){
                $users[$u->id] = $u->user->username;
            }
        }
        return $users;
    }
}
