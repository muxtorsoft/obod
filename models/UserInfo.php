<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_info}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $fio
 * @property string $lavozim
 * @property string $passport
 * @property string $buyruq
 * @property string $telefon
 *
 * @property User $user
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_info}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'fio'], 'required'],//, 'lavozim', 'buyruq', 'telefon'
            [['user_id'], 'integer'],
            [['passport'], 'string'],
            [['fio'], 'string', 'max' => 255],
            [['lavozim'], 'string', 'max' => 128],
            [['buyruq', 'telefon'], 'string', 'max' => 45],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'fio' => Yii::t('app', 'Fio'),
            'lavozim' => Yii::t('app', 'Lavozim'),
            'passport' => Yii::t('app', 'Passport'),
            'buyruq' => Yii::t('app', 'Buyruq'),
            'telefon' => Yii::t('app', 'Telefon'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
