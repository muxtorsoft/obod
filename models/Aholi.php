<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%aholi}}".
 *
 * @property int $id
 * @property int $locality_id
 * @property int $plan_id
 * @property int $user_id
 * @property int $aholi_soni
 * @property int $xonadon_soni
 * @property int $oila_soni
 *
 * @property Locality $locality
 * @property LocalityPlans $plan
 */
class Aholi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%aholi}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['locality_id', 'plan_id', 'user_id', 'aholi_soni', 'xonadon_soni', 'oila_soni'], 'integer'],
            [['plan_id'], 'required'],
            [['locality_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locality::className(), 'targetAttribute' => ['locality_id' => 'id']],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => LocalityPlans::className(), 'targetAttribute' => ['plan_id' => 'id']],
            [['aholi_soni', 'xonadon_soni', 'oila_soni'], 'required', 'on' => 'add'],
            ['plan_id', 'unique', 'targetAttribute' => ['plan_id'], 'message' => 'Plan must be unique.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'locality_id' => Yii::t('app', 'Locality ID'),
            'plan_id' => Yii::t('app', 'Plan ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'aholi_soni' => Yii::t('app', 'Aholi Soni'),
            'xonadon_soni' => Yii::t('app', 'Xonadon Soni'),
            'oila_soni' => Yii::t('app', 'Oila Soni'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocality()
    {
        return $this->hasOne(Locality::className(), ['id' => 'locality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(LocalityPlans::className(), ['id' => 'plan_id']);
    }
}
