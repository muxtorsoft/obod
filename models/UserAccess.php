<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user_access}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $region_id
 * @property string $type
 * @property int $to_id
 * @property int $create
 * @property int $update
 * @property int $view
 * @property int $delete
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class UserAccess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_access}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'region_id', 'to_id', 'create', 'update', 'view', 'delete', 'created_at', 'updated_at'], 'integer'],
            [['type'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'type' => Yii::t('app', 'Type'),
            'to_id' => Yii::t('app', 'To ID'),
            'create' => Yii::t('app', 'Create'),
            'update' => Yii::t('app', 'Update'),
            'view' => Yii::t('app', 'View'),
            'delete' => Yii::t('app', 'Delete'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        /** @var $access UserAccess */
        $access = UserAccess::find()->where(['user_id' => Yii::$app->user->getId()])->one();
        return $access != null ? $access->region_id : 0;
    }

    /**
     * @return null|string
     */
    public function getRegion()
    {
        return Region::findOne($this->getRegionId());
    }

    /**
     * @return null|string
     */
    public function getRegionTitle($delimit=null)
    {
        /** @var $region Region */
        $region = $this->getRegion();
        return $region != null ? $delimit.$region->title : null;
    }
}
