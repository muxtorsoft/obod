<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 22.02.2019
 * Time: 21:38
 */

namespace app\models;

/**
 * Class Users
 * @package app\models
 *
 * @property UserAccess[] $userAccesses
 */

class Users extends User
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccesses()
    {
        return $this->hasMany(UserAccess::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfo()
    {
        return $this->hasOne(UserInfo::className(), ['user_id' => 'id']);
    }
}