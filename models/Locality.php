<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%locality}}".
 *
 * @property int $id
 * @property int $district_id
 * @property string $title
 * @property string $description
 * @property string $type
 * @property int $sector_reg
 * @property int $sector_dis
 *
 * @property District $district
 * @property LocalityPlans[] $localityPlans
 */
class Locality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%locality}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['district_id', 'sector_reg', 'sector_dis'], 'integer'],
            [['title'], 'required'],
            [['description', 'type'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::className(), 'targetAttribute' => ['district_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'district_id' => Yii::t('app', 'District ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Type'),
            'sector_reg' => Yii::t('app', 'Sector region'),
            'sector_dis' => Yii::t('app', 'Sector district'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalityPlans()
    {
        return $this->hasMany(LocalityPlans::className(), ['locality_id' => 'id']);
    }
}
