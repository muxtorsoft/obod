<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Aholi */
/* @var $form yii\widgets\ActiveForm */
$plan = \app\models\LocalityPlans::findOne($model->plan_id);
?>

<div class="aholi-form">

    <?php $form = ActiveForm::begin([
        'id' => 'aholiform',
        'enableAjaxValidation' => true,
        'validationUrl' => \yii\helpers\Url::to(['fill-validate','plan_id'=>$model->plan_id]),
    ]); ?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $plan->locality->title?>га ахоли сонини кушиш</h4>
        <div class="warning text-danger">Диккат! Ушбу тулдириш формаси факат бир марта саклашга имкон беради! Шунинг учун тугри маълумот киритишингиз суралади!</div>
    </div>
    <div class="modal-body">
        <?php //= $form->field($model, 'plan_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\LocalityPlans::find()->all(), 'id', 'locality.title'), ['prompt' => 'Кишлок ёки махаллани танланг']) ?>

        <?= $form->field($model, 'aholi_soni')->textInput() ?>

        <?= $form->field($model, 'xonadon_soni')->textInput() ?>

        <?= $form->field($model, 'oila_soni')->textInput() ?>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app', 'Close')?></button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS

   $(document).ready(function () { 
        $("#aholiform").on('beforeSubmit', function (event) { 
            event.preventDefault();            
            var form_data = new FormData($('#aholiform')[0]);
            $.ajax({
                   url: $("#aholiform").attr('action'), 
                   dataType: 'JSON',  
                   cache: false,
                   contentType: false,
                   processData: false,
                   data: form_data, //$(this).serialize(),                      
                   type: 'post',                        
                   beforeSend: function() {
                   },
                   success: function(response){
                       $('#modal').modal('hide');
                       location.reload();
                   },
                   complete: function() {
                   },
                   error: function (data) {
                   }
                });                
            return false;
        });
    });       

JS;
$this->registerJs($script);
?>
