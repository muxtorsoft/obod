<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\LocalityPlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ахоли сони');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locality-plans-index">

    <h1><?= Html::a(Yii::t('app', 'Киритилганлари'), ['filled'], ['class' => 'pull-right btn btn-success']) ?><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'year',
            [
                'label' => 'Махалла ёки кишлок',
                'value' => 'locality.title'
            ],
            [
                'label' => 'Туман',
                'value' => 'locality.district.title'
            ],
            [
                'label' => 'Киритинг',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Yii::t('app', 'Киритиш!'),
                        ['fill', 'plan_id' => $model->id],
                        [
                            'title' => Yii::t('app', 'Ахоли сонини киритиш'),
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'class' => 'showModalButton btn btn-danger'
                        ]
                    );
                }
            ]

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<div class="modal remote fade" id="modal">
    <div class="modal-dialog">
        <div id="modalContent" class="modal-content loader-lg"></div>
    </div>
</div>
