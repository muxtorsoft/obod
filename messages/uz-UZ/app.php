<?php
/**
 * Created by PhpStorm.
 * User: Muxtorov Ulugbek
 * Date: 13.03.2019
 * Time: 12:28
 */
return [
    //Umumiy
    'Save' => 'Саклаш',
    'Close' => 'Ёпиш',
    'Login' => 'Кириш',
    'Model Categories' => 'Категориялар',
    'Models' => 'Эхтиёт кисмлар',
    'Users' => 'Фойдаланувчилар',
    'Jobs' => 'Килинган ишлар',
    'Year' => 'Йил',
    'Aholi Soni' => 'Ахоли сони',
    'Xonadon Soni' => 'Хонадон сони',
    'Oila Soni' => 'Оила сони',
];