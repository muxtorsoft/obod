<?php

use yii\db\Migration;

/**
 * Class m190325_135856_user_info
 */
class m190325_135856_user_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%user_info}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `fio` VARCHAR(255) NULL,
  `lavozim` VARCHAR(128) NULL,
  `passport` TEXT NULL,
  `buyruq` VARCHAR(45) NULL,
  `telefon` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `ui_user` (`user_id` ASC),
  CONSTRAINT `fk_ui_user`
    FOREIGN KEY (`user_id`)
    REFERENCES {{%user}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190325_135856_user_info cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190325_135856_user_info cannot be reverted.\n";

        return false;
    }
    */
}
