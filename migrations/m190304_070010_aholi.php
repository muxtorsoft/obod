<?php

use yii\db\Migration;

/**
 * Class m190304_070010_aholi
 */
class m190304_070010_aholi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS {{%aholi}} (
  `id` INT NOT NULL AUTO_INCREMENT,
  `locality_id` INT NULL,
  `plan_id` INT NOT NULL,
  `user_id` INT NULL,
  `aholi_soni` INT NULL,
  `xonadon_soni` INT NULL,
  `oila_soni` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `locality` (`locality_id` ASC),
  INDEX `plan` (`plan_id` ASC),
  CONSTRAINT `fk_aholi_locality`
    FOREIGN KEY (`locality_id`)
    REFERENCES {{%locality}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_aholi_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES {{%locality_plans}} (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190304_070010_aholi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190304_070010_aholi cannot be reverted.\n";

        return false;
    }
    */
}
