<?php

use yii\db\Migration;

/**
 * Class m190403_061508_alter_locality_sector
 */
class m190403_061508_alter_locality_sector extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%locality}}
	ADD COLUMN `sector_reg` INT NULL DEFAULT NULL AFTER `type`,
	ADD COLUMN `sector_dis` INT NULL DEFAULT NULL AFTER `sector_reg`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190403_061508_alter_locality_sector cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190403_061508_alter_locality_sector cannot be reverted.\n";

        return false;
    }
    */
}
