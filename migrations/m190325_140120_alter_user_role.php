<?php

use yii\db\Migration;

/**
 * Class m190325_140120_alter_user_role
 */
class m190325_140120_alter_user_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{%user}}
	ADD COLUMN `role` ENUM('admin','moderator','region','view') NULL DEFAULT NULL AFTER `status`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190325_140120_alter_user_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190325_140120_alter_user_role cannot be reverted.\n";

        return false;
    }
    */
}
