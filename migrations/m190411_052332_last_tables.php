<?php

use yii\db\Migration;

/**
 * Class m190411_052332_last_tables
 */
class m190411_052332_last_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `moliya_manbai` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `moliya` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `obod_reja` DECIMAL NULL,
  `resp_reja` DECIMAL NULL,
  `mah_byud_reja` DECIMAL NULL,
  `xalqaro_reja` DECIMAL NULL,
  `tarmoq_reja` DECIMAL NULL,
  `mah_jam_reja` DECIMAL NULL,
  `tashabbus_reja` DECIMAL NULL,
  `hashar_reja` DECIMAL NULL,
  `tij_bank_reja` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `moliya_plan` (`plan_id` ASC),
  CONSTRAINT `fk_moliya_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида қурилиш ва ободонлаштириш ишларининг молиялаштирилиш ҳолати тўғрисида';

CREATE TABLE IF NOT EXISTS `uyfondi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `yakka_son_reja` DECIMAL NULL,
  `yakka_qiy_reja` DECIMAL NULL,
  `kup_son_reja` DECIMAL NULL,
  `kup_qiy_reja` DECIMAL NULL,
  `yulka_km_reja` DECIMAL NULL,
  `yulka_qiy_reja` DECIMAL NULL,
  `kucha_dona_reja` DECIMAL NULL,
  `kucha_qiy_reja` DECIMAL NULL,
  `gul_km_reja` DECIMAL NULL,
  `gul_qiy_reja` DECIMAL NULL,
  `boshqa_dona_reja` DECIMAL NULL,
  `boshqa_qiy_reja` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `uyfondi_plan` (`plan_id` ASC),
  CONSTRAINT `fk_uyfondi_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида уй-жой фонди, ободонлаштириш ва кўкаламзорлаштириш ишлари';

CREATE TABLE IF NOT EXISTS `moliya_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `moliya_id` INT NOT NULL,
  `obod_fakt` DECIMAL NULL,
  `resp_fakt` DECIMAL NULL,
  `mah_byud_fakt` DECIMAL NULL,
  `xalqaro_fakt` DECIMAL NULL,
  `tarmoq_fakt` DECIMAL NULL,
  `mah_jam_fakt` DECIMAL NULL,
  `tashabbus_fakt` DECIMAL NULL,
  `hashar_fakt` DECIMAL NULL,
  `tij_bank_fakt` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `moliya_fakt` (`moliya_id` ASC),
  CONSTRAINT `fk_moliya_fakt`
    FOREIGN KEY (`moliya_id`)
    REFERENCES `moliya` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `uyfondi_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uyfond_id` INT NOT NULL,
  `yakka_son_fakt` DECIMAL NULL,
  `yakka_qiy_fakt` DECIMAL NULL,
  `kup_son_fakt` DECIMAL NULL,
  `kup_qiy_fakt` DECIMAL NULL,
  `yulka_km_fakt` DECIMAL NULL,
  `yulka_qiy_fakt` DECIMAL NULL,
  `kucha_dona_fakt` DECIMAL NULL,
  `kucha_qiy_fakt` DECIMAL NULL,
  `gul_km_fakt` DECIMAL NULL,
  `gul_qiy_fakt` DECIMAL NULL,
  `boshqa_dona_fakt` DECIMAL NULL,
  `boshqa_qiy_fakt` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `uyfondi_fakt` (`uyfond_id` ASC),
  CONSTRAINT `fk_uyfondi_fakt`
    FOREIGN KEY (`uyfond_id`)
    REFERENCES `uyfondi` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `energiya` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `elektr_km_reja` DECIMAL NULL,
  `elektr_qiy_reja` DECIMAL NULL,
  `elektr_stan_reja` DECIMAL NULL,
  `tgaz_km_reja` DECIMAL NULL,
  `tgaz_qiy_reja` DECIMAL NULL,
  `sgaz_son_reja` DECIMAL NULL,
  `sgaz_qiy_reja` DECIMAL NULL,
  `kumir_hajm_reja` DECIMAL NULL,
  `kumir_qiy_reja` DECIMAL NULL,
  `suv_km_reja` DECIMAL NULL,
  `suv_qiy_reja` DECIMAL NULL,
  `toza_son_reja` DECIMAL NULL,
  `toza_qiy_reja` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `energiya_plan` (`plan_id` ASC),
  CONSTRAINT `fk_energiya_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида электр энергия, табиий газ, суюлтирилган газ, кўмир, ичимлик суви таъминоти ва тозалов иншоотлари бўйича амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `yul` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `avto_km_reja` DECIMAL NULL,
  `avto_qiy_reja` DECIMAL NULL,
  `bus_dona_reja` INT NULL,
  `bus_qiy_reja` DECIMAL NULL,
  `bekat_dona_reja` INT NULL,
  `bekat_qiy_reja` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yul_plan` (`plan_id` ASC),
  CONSTRAINT `fk_yul_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида йўл-транспорт инфратузилма объектларини қуриш ва таъмирлаш ишлари тўғрисида';

CREATE TABLE IF NOT EXISTS `ijtimoiy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `ijtimoi_son_reja` INT NULL,
  `ijtimoi_qiy_reja` DECIMAL NULL,
  `maktab_son_reja` INT NULL,
  `maktab_qiy_reja` DECIMAL NULL,
  `boqcha_son_reja` INT NULL,
  `boqcha_qiy_reja` DECIMAL NULL,
  `tib_son_reja` INT NULL,
  `tib_qiy_reja` DECIMAL NULL,
  `mfy_son_reja` INT NULL,
  `mfy_qiy_reja` DECIMAL NULL,
  `boshqa_son_reja` INT NULL,
  `boshqa_qiy_reja` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `ijtimoiy_plan` (`plan_id` ASC),
  CONSTRAINT `fk_ijtimoiy_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида ижтимоий соҳа объектлари бўйича амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `bozproject` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `title` VARCHAR(255) NULL,
  `description` TEXT NULL,
  `uzmab_reja` DECIMAL NULL,
  `kredit_reja` DECIMAL NULL,
  `boshqa_reja` DECIMAL NULL,
  `ish_reja` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_bozproject_plan_idx` (`plan_id` ASC),
  CONSTRAINT `fk_bozproject_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида бозор инфратузилмаси объектларини таъмирлаш ва қуриш бўйича амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `qurmaterial` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `metal_reja` DECIMAL NULL,
  `cment_reja` DECIMAL NULL,
  `shifer_reja` INT NULL,
  `gisht_reja` INT NULL,
  `shagal_reja` DECIMAL NULL,
  `qum_reja` DECIMAL NULL,
  `yogoc_reja` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `qurmat_plan` (`plan_id` ASC),
  CONSTRAINT `fk_qurmat_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида етказиб берилаётган асосий турдаги қурилиш ва пардозлаш материаллари тўғрисида ';

CREATE TABLE IF NOT EXISTS `yakkauy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `tom_son_reja` INT NULL,
  `tom_kvm_reja` DECIMAL NULL,
  `tom_qiy_reja` DECIMAL NULL,
  `tom_qiy_moliya` INT NULL COMMENT 'молиялаштириш манбаи',
  `tom_qiy_molled` DECIMAL NULL,
  `fasad_son_reja` INT NULL,
  `fasad_kvm_reja` DECIMAL NULL,
  `fasad_qiy_reja` DECIMAL NULL,
  `fasad_qiy_moliya` INT NULL COMMENT 'молиялаштириш манбаи',
  `fasad_qiy_molled` DECIMAL NULL,
  `darvoza_son_reja` INT NULL,
  `darvoza_qiy_reja` DECIMAL NULL,
  `darvoza_qiy_moliya` INT NULL COMMENT 'молиялаштириш манбаи',
  `darvoza_qiy_molled` DECIMAL NULL,
  `hojat_son_reja` INT NULL,
  `hojat_qiy_reja` DECIMAL NULL,
  `hojat_qiy_moliya` INT NULL,
  `hojat_qiy_molled` DECIMAL NULL,
  `tusiq_miq_reja` DECIMAL NULL,
  `tusiq_qiy_reja` DECIMAL NULL,
  `tusiq_qiy_moliya` INT NULL,
  `tusiq_qiy_molled` DECIMAL NULL,
  `tusiq_tamir_miq_reja` DECIMAL NULL,
  `tusiq_tamir_qiy_reja` DECIMAL NULL,
  `tusiq_tamir_qiy_moliya` INT NULL,
  `tusiq_tamir_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yakkauy_plan` (`plan_id` ASC),
  CONSTRAINT `fk_yakkauy_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида якка тартибдаги уй-жойларни таъмирлаш борасида амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `qishobod` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `tratuar_km_reja` DECIMAL NULL,
  `tratuar_qiy_moliya` INT NULL,
  `tratuar_qiy_reja` DECIMAL NULL,
  `diod_dona_reja` INT NULL,
  `diod_qiy_moliya` INT NULL,
  `diod_qiy_reja` DECIMAL NULL,
  `gulzor_km_reja` DECIMAL NULL,
  `gulzor_qiy_moliya` INT NULL,
  `gulzor_qiy_reja` DECIMAL NULL,
  `boshqa_dona_reja` DECIMAL NULL,
  `boshqa_qiy_moliya` INT NULL,
  `boshqa_qiy_reja` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `qishobob_plan` (`plan_id` ASC),
  CONSTRAINT `fk_qishobod_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида қишлоқларда ободонлаштириш борасида амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `elektr` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `uyangi_dona_reja` INT NULL,
  `uyangi_qiy_reja` DECIMAL NULL,
  `uyangi_molled` DECIMAL NULL,
  `ualma_dona_reja` INT NULL,
  `ualma_qiy_reja` DECIMAL NULL,
  `ualma_qiy_molled` DECIMAL NULL,
  `urek_dona_reja` INT NULL,
  `urek_qiy_reja` DECIMAL NULL,
  `urek_qiy_molled` DECIMAL NULL,
  `eyangi_dona_reja` INT NULL,
  `eyangi_qiy_reja` DECIMAL NULL,
  `eyangi_qiy_molled` DECIMAL NULL,
  `ealm_dona_reja` INT NULL,
  `ealm_qiy_reja` DECIMAL NULL,
  `ealm_qiy_molled` DECIMAL NULL,
  `erek_dona_reja` INT NULL,
  `erek_qiy_reja` DECIMAL NULL,
  `erek_qiy_molled` DECIMAL NULL,
  `tyangi_dona_reja` INT NULL,
  `tyangi_qiy_reja` DECIMAL NULL,
  `tyangi_qiy_molled` DECIMAL NULL,
  `talm_dona_reja` INT NULL,
  `talm_qiy_reja` DECIMAL NULL,
  `talm_qiy_molled` DECIMAL NULL,
  `trek_dona_reja` INT NULL,
  `trek_qiy_reja` DECIMAL NULL,
  `trek_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `elektr_plan` (`plan_id` ASC),
  CONSTRAINT `fk_elektr_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида электр таъминотини яхшилаш борасида амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `gaz` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `qyot_km_reja` DECIMAL NULL,
  `qyot_qiy_reja` DECIMAL NULL,
  `qyot_qiy_molled` DECIMAL NULL,
  `qtam_km_reja` DECIMAL NULL,
  `qtam_qiy_reja` DECIMAL NULL,
  `qtam_qiy_molled` DECIMAL NULL,
  `qdem_km_reja` DECIMAL NULL,
  `qdem_qiy_reja` DECIMAL NULL,
  `qdem_qiy_molled` DECIMAL NULL,
  `pyangi_dona_reja` INT NULL,
  `pyangi_qiy_reja` DECIMAL NULL,
  `pyangi_qiy_molled` DECIMAL NULL,
  `prek_dona_reja` INT NULL,
  `prek_qiy_reja` DECIMAL NULL,
  `prek_qiy_molled` DECIMAL NULL,
  `balon_dona_reja` INT NULL,
  `balon_qiy_reja` DECIMAL NULL,
  `balon_qiy_molled` DECIMAL NULL,
  `kumir_tn_reja` DECIMAL NULL,
  `kumir_qiy_reja` DECIMAL NULL,
  `kumir_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `gaz_plan` (`plan_id` ASC),
  CONSTRAINT `fk_gaz_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида газ таъминотини яхшилаш борасида амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `yakkauyhudud` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `yakkauyh_plan` (`plan_id` ASC),
  CONSTRAINT `fk_yakkauyh_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида якка тартибдаги уй-жойларни таъмирлаш ва ҳудудларини ободонлаштириш борасида амалга оширилаётган ишлар тўғрисида																				\n';

CREATE TABLE IF NOT EXISTS `yuh_tom` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `tom_son` INT NULL,
  `tom_kvm` DECIMAL NULL,
  `tom_qiy` DECIMAL NULL,
  `tom_qiy_moliya` INT NULL,
  `tom_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_tom` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_tom`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_tom_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_tom_id` INT NULL,
  `tom_son_amalda` INT NULL,
  `tom_kvm_amalda` DECIMAL NULL,
  `tom_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_tom_amalda` (`yuh_tom_id` ASC),
  CONSTRAINT `fk_yuh_tom_amalda`
    FOREIGN KEY (`yuh_tom_id`)
    REFERENCES `yuh_tom` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_fasad` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `fasad_son` INT NULL,
  `fasad_kvm` DECIMAL NULL,
  `fasad_qiy` DECIMAL NULL,
  `fasad_qiy_moliya` INT NULL,
  `fasad_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_fasad` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_fasad`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_fasad_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_fasad_id` INT NULL,
  `fasad_son_amalda` INT NULL,
  `fasad_kvm_amalda` DECIMAL NULL,
  `fasad_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_fasad_amalda` (`yuh_fasad_id` ASC),
  CONSTRAINT `fk_yuh_fasad_amalda`
    FOREIGN KEY (`yuh_fasad_id`)
    REFERENCES `yuh_fasad` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_kiryul` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `kiryul_son` INT NULL,
  `kiryul_qiy_reja` DECIMAL NULL,
  `kiryul_qiy_moliya` INT NULL,
  `kiryul_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_kiryul` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_kiryul`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_kiryul_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_kiryul_id` INT NULL,
  `kiryul_son_amalda` INT NULL,
  `kiryul_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_kiryul_amalda` (`yuh_kiryul_id` ASC),
  CONSTRAINT `fk_yuh_kiryul_amalda`
    FOREIGN KEY (`yuh_kiryul_id`)
    REFERENCES `yuh_kiryul` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_temireshik` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `teshik_son` INT NULL,
  `teshik_qiy` DECIMAL NULL,
  `teshik_qiy_moliya` INT NULL,
  `teshik_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_temireshik` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_temireshik`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_temireshik_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_temireshik_id` INT NULL,
  `teshik_son_amalda` INT NULL,
  `teshik_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_temireshik_amalda` (`yuh_temireshik_id` ASC),
  CONSTRAINT `fk_yuh_temireshik_amalda`
    FOREIGN KEY (`yuh_temireshik_id`)
    REFERENCES `yuh_temireshik` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_ekqulf` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `kqulf_son` INT NULL,
  `kqulf_qiy` DECIMAL NULL,
  `kqulf_qiy_moliya` INT NULL,
  `kqulf_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_ekqulf` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_ekqulf`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_ekqulf_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_ekqulf_id` INT NULL,
  `kqulf_son_amalda` INT NULL,
  `kqulf_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_ekqulf_amalda` (`yuh_ekqulf_id` ASC),
  CONSTRAINT `fk_yuh_ekqulf_amalda`
    FOREIGN KEY (`yuh_ekqulf_id`)
    REFERENCES `yuh_ekqulf` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_kozirek` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `kozirek_son` INT NULL,
  `kozirek_qiy` DECIMAL NULL,
  `kozirek_qiy_moliya` INT NULL,
  `kozirek_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_kozirek` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_kozirek`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_kozirek_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_kozirek_id` INT NULL,
  `kozirek_son_amalda` INT NULL,
  `kozirek_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_kozirek_amalda` (`yuh_kozirek_id` ASC),
  CONSTRAINT `fk_yuh_kozirek_amalda`
    FOREIGN KEY (`yuh_kozirek_id`)
    REFERENCES `yuh_kozirek` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `yuh_framuga` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `fram_kvm` DECIMAL NULL,
  `fram_qiy` DECIMAL NULL,
  `fram_qiy_moliya` INT NULL,
  `fram_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_framuga` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_framuga`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_framuga_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_framuga_id` INT NULL,
  `fram_kvm_amalda` DECIMAL NULL,
  `fram_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_framuga_amalda` (`yuh_framuga_id` ASC),
  CONSTRAINT `fk_yuh_framuga_amalda`
    FOREIGN KEY (`yuh_framuga_id`)
    REFERENCES `yuh_framuga` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_tarnov` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `tarnov_pm` DECIMAL NULL,
  `tarnov_qiy` DECIMAL NULL,
  `tarnov_qiy_moliya` INT NULL,
  `tarnov_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_tarnov` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_tarnov`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_tarnov_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_tarnov_id` INT NULL,
  `tarnov_pm_amalda` DECIMAL NULL,
  `tarnov_qi_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_tarnov_amalda` (`yuh_tarnov_id` ASC),
  CONSTRAINT `fk_yuh_tarnov_amalda`
    FOREIGN KEY (`yuh_tarnov_id`)
    REFERENCES `yuh_tarnov` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_yondevor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `yondevor_kvm` DECIMAL NULL,
  `yondevor_qiy` DECIMAL NULL,
  `yondevor_qiy_moliya` INT NULL,
  `yondevor_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_yondevor` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_yondevor`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_yondevor_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_yondevor_id` INT NULL,
  `yondevor_kvm_amalda` DECIMAL NULL,
  `yondevor_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_yondevor_amalda` (`yuh_yondevor_id` ASC),
  CONSTRAINT `fk_yuh_yondevor_amalda`
    FOREIGN KEY (`yuh_yondevor_id`)
    REFERENCES `yuh_yondevor` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_tandir` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `tandir_son` INT NULL,
  `tandir_qiy` DECIMAL NULL,
  `tandir_qiy_moliya` INT NULL,
  `tandir_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_tandir` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_tandir`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_tandir_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_tandir_id` INT NULL,
  `tandir_son_amalda` INT NULL,
  `tandir_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_tandir_amalda` (`yuh_tandir_id` ASC),
  CONSTRAINT `fk_yuh_tandir_amalda`
    FOREIGN KEY (`yuh_tandir_id`)
    REFERENCES `yuh_tandir` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_eshit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `shit_son` INT NULL,
  `shit_qiy` DECIMAL NULL,
  `shit_qiy_moliya` INT NULL,
  `shit_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_eshit` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_eshit`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_eshit_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_eshit_id` INT NULL,
  `shit_son_amalda` INT NULL,
  `shit_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_eshit_amalda` (`yuh_eshit_id` ASC),
  CONSTRAINT `fk_yuh_eshit_amalda`
    FOREIGN KEY (`yuh_eshit_id`)
    REFERENCES `yuh_eshit` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_istikquv` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `quv_pm` DECIMAL NULL,
  `quv_qiy` DECIMAL NULL,
  `quv_qiy_moliya` INT NULL,
  `quv_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_istikquv` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_istikquv`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_istikquv_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_istikquv` INT NULL,
  `quv_pm_amalda` DECIMAL NULL,
  `quv_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_istikquv_amalda` (`yuh_istikquv` ASC),
  CONSTRAINT `fk_yuh_istikquv_amalda`
    FOREIGN KEY (`yuh_istikquv`)
    REFERENCES `yuh_istikquv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_ertulakom` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `ertula_son` INT NULL,
  `ertula_qiy` DECIMAL NULL,
  `ertula_qiy_moliya` INT NULL,
  `ertula_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_ertulakom` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_ertulakom`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_ertulakom_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_ertulakom_id` INT NULL,
  `ertula_son_amalda` INT NULL,
  `ertula_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_ertulakom_amalda` (`yuh_ertulakom_id` ASC),
  CONSTRAINT `fk_yuh_ertulakom_amalda`
    FOREIGN KEY (`yuh_ertulakom_id`)
    REFERENCES `yuh_ertulakom` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_bolmaydon` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `maydon_son` INT NULL,
  `maydon_qiy` DECIMAL NULL,
  `maydon_qiy_moliya` INT NULL,
  `maydon_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_bolmaydon` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_bolmaydon`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_bolmaydon_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_bolmaydon_id` INT NULL,
  `maydon_son_amalda` INT NULL,
  `maydon_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_bolmaydon_amalda` (`yuh_bolmaydon_id` ASC),
  CONSTRAINT `fk_yuh_bolmaydon_amalda`
    FOREIGN KEY (`yuh_bolmaydon_id`)
    REFERENCES `yuh_bolmaydon` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_xojatxona` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `x_son` INT NULL,
  `x_qiy` DECIMAL NULL,
  `x_qiy_moliya` INT NULL,
  `x_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_xojatxona` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_xojatxona`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_xojatxona_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_xojatxona_id` INT NULL,
  `x_son_amalda` INT NULL,
  `x_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_xojatxona_amalda` (`yuh_xojatxona_id` ASC),
  CONSTRAINT `fk_yuh_xojatxona_amalda`
    FOREIGN KEY (`yuh_xojatxona_id`)
    REFERENCES `yuh_xojatxona` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_kanalizatsiya` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `kan_son` INT NULL,
  `kan_qiy` DECIMAL NULL,
  `kan_qiy_moliya` INT NULL,
  `kan_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_kanalizatsiya` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_kanalizatsiya`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_kanalizatsiya_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_kanalizatsiya_id` INT NULL,
  `kan_son_amalda` INT NULL,
  `kan_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_kanalizatsiya_amalda` (`yuh_kanalizatsiya_id` ASC),
  CONSTRAINT `fk_yuh_kanalizatsiya_amalda`
    FOREIGN KEY (`yuh_kanalizatsiya_id`)
    REFERENCES `yuh_kanalizatsiya` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_irrigatsiya` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `ir_son` INT NULL,
  `ir_qiy` DECIMAL NULL,
  `ir_qiy_moliya` INT NULL,
  `ir_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_irrigatsiya` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_irrigatsiya`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_irrigatsiya_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_irrigatsiya_id` INT NULL,
  `ir_son_amalda` INT NULL,
  `ir_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_irrigatsiya_amalda` (`yuh_irrigatsiya_id` ASC),
  CONSTRAINT `fk_yuh_irrigatsiya_amalda`
    FOREIGN KEY (`yuh_irrigatsiya_id`)
    REFERENCES `yuh_irrigatsiya` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_trubomost` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `tmost_pm` DECIMAL NULL,
  `tmost_qiy` DECIMAL NULL,
  `tmost_moliya` INT NULL,
  `tmost_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_trubomost` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_trubomost`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_trubomost_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_trubomost_id` INT NULL,
  `tmost_son_amalda` INT NULL,
  `tmost_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_trubomost_amalda` (`yuh_trubomost_id` ASC),
  CONSTRAINT `fk_yuh_trubomost_amalda`
    FOREIGN KEY (`yuh_trubomost_id`)
    REFERENCES `yuh_trubomost` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_garaj` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `garaj_son` INT NULL,
  `garaj_qiy` DECIMAL NULL,
  `garaj_qiy_moliya` INT NULL,
  `garaj_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_garaj` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_garaj`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_garaj_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_garaj_id` INT NULL,
  `garaj_son_amalda` INT NULL,
  `garaj_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_garaj_amalda` (`yuh_garaj_id` ASC),
  CONSTRAINT `fk_yuh_garaj_amalda`
    FOREIGN KEY (`yuh_garaj_id`)
    REFERENCES `yuh_garaj` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_trotuar` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `trotuar_son` INT NULL,
  `trotuar_qiy` DECIMAL NULL,
  `trotuar_qiy_moliya` INT NULL,
  `trotuar_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_trotuar` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_trotuar`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_trotuar_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_trotuar_id` INT NULL,
  `trotuar_son_amalda` INT NULL,
  `trotuar_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_trotuar_amalda` (`yuh_trotuar_id` ASC),
  CONSTRAINT `fk_yuh_trotuar_amalda`
    FOREIGN KEY (`yuh_trotuar_id`)
    REFERENCES `yuh_trotuar` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_otmaska` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `otmaska_kvm` DECIMAL NULL,
  `otmaska_qiy` DECIMAL NULL,
  `otmaska_qiy_moliya` INT NULL,
  `otmaska_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_otmaska` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_otmaska`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_otmaska_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_otmaska_id` INT NULL,
  `otmaska_kvm_amalda` DECIMAL NULL,
  `otmaska_qiy_fused` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_otmaska_amalda` (`yuh_otmaska_id` ASC),
  CONSTRAINT `fk_yuh_otmaska_amalda`
    FOREIGN KEY (`yuh_otmaska_id`)
    REFERENCES `yuh_otmaska` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_bordyur` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `bordyur_son` INT NULL,
  `bordyur_qiy` DECIMAL NULL,
  `bordyur_qiy_moliya` INT NULL,
  `bordyur_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_bordyur` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_bordyur`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_bordyur_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_bordyur_id` INT NULL,
  `bordyur_son_amalda` INT NULL,
  `bordyur_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_bordyur_amalda` (`yuh_bordyur_id` ASC),
  CONSTRAINT `fk_yuh_bordyur_amalda`
    FOREIGN KEY (`yuh_bordyur_id`)
    REFERENCES `yuh_bordyur` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_uyraqam` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `uraqam_son` INT NULL,
  `uraqam_qiy` DECIMAL NULL,
  `uraqam_qiy_moliya` INT NULL,
  `uraqam_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_uyraqam` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_uyraqam`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_uyraqam_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_uyraqam_id` INT NULL,
  `uraqam_son_amalda` INT NULL,
  `uraqam_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_uyraqam_amalda` (`yuh_uyraqam_id` ASC),
  CONSTRAINT `fk_yuh_uyraqam_amalda`
    FOREIGN KEY (`yuh_uyraqam_id`)
    REFERENCES `yuh_uyraqam` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_kamera` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `kamera_son` INT NULL,
  `kamera_qiy` DECIMAL NULL,
  `kamera_qiy_moliya` INT NULL,
  `kamera_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_kamera` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_kamera`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_kamera_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_kamera_id` INT NULL,
  `kamera_son_amalda` INT NULL,
  `kamera_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_kamera_amalda` (`yuh_kamera_id` ASC),
  CONSTRAINT `fk_yuh_kamera_amalda`
    FOREIGN KEY (`yuh_kamera_id`)
    REFERENCES `yuh_kamera` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_chiroq` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `chiroq_son` INT NULL,
  `chiroq_qiy` DECIMAL NULL,
  `chiroq_qiy_moliya` INT NULL,
  `chiroq_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_chiroq` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_chiroq`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_chiroq_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_chiroq_id` INT NULL,
  `chiroq_son_amalda` INT NULL,
  `chiroq_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_chiroq_amalda` (`yuh_chiroq_id` ASC),
  CONSTRAINT `fk_yuh_chiroq_amalda`
    FOREIGN KEY (`yuh_chiroq_id`)
    REFERENCES `yuh_chiroq` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_buzish` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_id` INT NULL,
  `buzish_son` INT NULL,
  `buzish_qiy` DECIMAL NULL,
  `buzish_qiy_moliya` INT NULL,
  `buzish_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_buzish` (`yuh_id` ASC),
  CONSTRAINT `fk_yuh_buzish`
    FOREIGN KEY (`yuh_id`)
    REFERENCES `yakkauyhudud` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yuh_buzish_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yuh_buzish_id` INT NULL,
  `buzish_son_amalda` INT NULL,
  `buzish_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yuh_buzish_amalda` (`yuh_buzish_id` ASC),
  CONSTRAINT `fk_yuh_buzish_amalda`
    FOREIGN KEY (`yuh_buzish_id`)
    REFERENCES `yuh_buzish` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ichimliksuv` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `plan_ichimliksuv` (`plan_id` ASC),
  CONSTRAINT `fk_plan_ichimliksuv`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида ичимлик сув таъминотини яхшилаш борасида амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `isuv_tarmoq_yangi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `yangi_miqdor` DECIMAL NULL,
  `yangi_qiy` DECIMAL NULL,
  `yangi_qiy_moliya` INT NULL,
  `yangi_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_tarmoq_yangi` (`is_id` ASC),
  CONSTRAINT `fk_isuv_tarmoq_yangi`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_tarmoq_yangi_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_tarmoq_id` INT NULL,
  `yangi_miqdor_amalda` DECIMAL NULL,
  `yangi_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_tarmoq_yangi_amalda` (`is_tarmoq_id` ASC),
  CONSTRAINT `fk_isuv_tarmoq_yangi_amalda`
    FOREIGN KEY (`is_tarmoq_id`)
    REFERENCES `isuv_tarmoq_yangi` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_tarmoq_rek` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `rek_miqdor` DECIMAL NULL,
  `rek_qiy` DECIMAL NULL,
  `rek_qiy_moliya` INT NULL,
  `rek_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_tarmoq_rek` (`is_id` ASC),
  CONSTRAINT `fk_isuv_tarmoq_rek`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_tarmoq_rek_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tarmoq_rek_id` INT NULL,
  `rek_miqdor_amalda` DECIMAL NULL,
  `rek_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_tarmoq_rek_amalda` (`tarmoq_rek_id` ASC),
  CONSTRAINT `fk_isuv_tarmoq_rek_amalda`
    FOREIGN KEY (`tarmoq_rek_id`)
    REFERENCES `isuv_tarmoq_rek` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_tarmoq_mukam` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `mukam_miqdor` DECIMAL NULL,
  `mukam_qiy` DECIMAL NULL,
  `mukam_qiy_moliya` INT NULL,
  `mukam_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_tarmoq_mukam` (`is_id` ASC),
  CONSTRAINT `fk_isuv_tarmoq_mukam`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_tarmoq_mukam_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mukam_id` INT NULL,
  `mukam_miqdor_amalda` DECIMAL NULL,
  `mukam_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_tarmoq_mukam_amalda` (`mukam_id` ASC),
  CONSTRAINT `fk_isuv_tarmoq_mukam_amalda`
    FOREIGN KEY (`mukam_id`)
    REFERENCES `isuv_tarmoq_mukam` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_inshoot_qurish` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `qurish_miqdor` INT NULL,
  `qurish_qiy` DECIMAL NULL,
  `qurish_qiy_moliya` INT NULL,
  `qurish_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_inshoot_qurish` (`is_id` ASC),
  CONSTRAINT `fk_isuv_inshoot_qurish`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_inshoot_qurish_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `qurish_id` INT NULL,
  `qurish_miqdor_amalda` INT NULL,
  `qurish_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_inshoot_qurish_amalda` (`qurish_id` ASC),
  CONSTRAINT `fk_isuv_inshoot_qurish_amalda`
    FOREIGN KEY (`qurish_id`)
    REFERENCES `isuv_inshoot_qurish` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_inshoot_rek` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `rek_miqdor` INT NULL,
  `rek_qiy` DECIMAL NULL,
  `rek_qiy_moliya` INT NULL,
  `rek_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_inshoot_rek` (`is_id` ASC),
  CONSTRAINT `fk_isuv_inshoot_rek`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_inshoot_rek_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `inshoot_rek_id` INT NULL,
  `rek_miqdor_amalda` INT NULL,
  `rek_miqdor_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_inshoot_rek_amalda` (`inshoot_rek_id` ASC),
  CONSTRAINT `fk_isuv_inshoot_rek_amalda`
    FOREIGN KEY (`inshoot_rek_id`)
    REFERENCES `isuv_inshoot_rek` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_inshoot_mukam` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `mukam_miqdor` INT NULL,
  `mukam_qiy` DECIMAL NULL,
  `mukam_qiy_moliya` INT NULL,
  `mukam_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_inshoot_mukam` (`is_id` ASC),
  CONSTRAINT `fk_isuv_inshoot_mukam`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_inshoot_mukam_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mukam_id` INT NULL,
  `mukam_miqdor_amalda` INT NULL,
  `mukam_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_inshoot_mukam_amalda` (`mukam_id` ASC),
  CONSTRAINT `fk_isuv_inshoot_mukam_amalda`
    FOREIGN KEY (`mukam_id`)
    REFERENCES `isuv_inshoot_mukam` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_transfor_yangi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `yangi_midqor` INT NULL,
  `yangi_qiy` DECIMAL NULL,
  `yangi_qiy_moliya` INT NULL,
  `yangi_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_transfor_yangi` (`is_id` ASC),
  CONSTRAINT `fk_isuv_transfor_yangi`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_transfor_yangi_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `isuv_tranfor_id` INT NULL,
  `yangi_miqdor_amalda` INT NULL,
  `yangi_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_transfor_yangi_amalda` (`isuv_tranfor_id` ASC),
  CONSTRAINT `fk_isuv_transfor_yangi_amalda`
    FOREIGN KEY (`isuv_tranfor_id`)
    REFERENCES `isuv_transfor_yangi` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_transfor_rek` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `rek_miqdor` INT NULL,
  `rek_qiy` DECIMAL NULL,
  `rek_qiy_moliya` INT NULL,
  `rek_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_transfor_rek` (`is_id` ASC),
  CONSTRAINT `fk_isuv_transfor_rek`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_transfor_rek_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `trans_rek_id` INT NULL,
  `rek_miqdor_amalda` INT NULL,
  `rek_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_transfor_rek_amalda` (`trans_rek_id` ASC),
  CONSTRAINT `fk_isuv_transfor_rek_amalda`
    FOREIGN KEY (`trans_rek_id`)
    REFERENCES `isuv_transfor_rek` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_transfor_mukam` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `mukam_miqdor` INT NULL,
  `mukam_qiy` DECIMAL NULL,
  `mukam_qiy_moliya` INT NULL,
  `mukam_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_transfor_mukam` (`is_id` ASC),
  CONSTRAINT `fk_isuv_transfor_mukam`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_transfor_mukam_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `trans_mukam_id` INT NULL,
  `mukam_miqdor_amalda` INT NULL,
  `mukam_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_transfor_mukam_amalda` (`trans_mukam_id` ASC),
  CONSTRAINT `fk_isuv_transfor_mukam_amalda`
    FOREIGN KEY (`trans_mukam_id`)
    REFERENCES `isuv_transfor_mukam` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_minora_yangi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `yangi_miqdor` INT NULL,
  `yangi_rek` DECIMAL NULL,
  `yangi_qiy` DECIMAL NULL,
  `yangi_qiy_moliya` INT NULL,
  `yangi_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_minora_yangi` (`is_id` ASC),
  CONSTRAINT `fk_isuv_minora_yangi`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_minora_yangi_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `minor_yangi_id` INT NULL,
  `yangi_miqdor_amalda` INT NULL,
  `yangi_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_minora_yangi_amalda` (`minor_yangi_id` ASC),
  CONSTRAINT `fk_isuv_minora_yangi_amalda`
    FOREIGN KEY (`minor_yangi_id`)
    REFERENCES `isuv_minora_yangi` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_minor_rek` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `rek_miqdor` INT NULL,
  `rek_qiy` DECIMAL NULL,
  `rek_qiy_moliya` INT NULL,
  `rek_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_minor_rek` (`is_id` ASC),
  CONSTRAINT `fk_isuv_minor_rek`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_minor_rek_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `minor_rek_id` INT NULL,
  `rek_miqdor_amalda` INT NULL,
  `rek_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_minor_rek_amalda` (`minor_rek_id` ASC),
  CONSTRAINT `fk_isuv_minor_rek_amalda`
    FOREIGN KEY (`minor_rek_id`)
    REFERENCES `isuv_minor_rek` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_minor_mukam` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `mukam_miqdor` INT NULL,
  `mukam_qiy` DECIMAL NULL,
  `mukam_qiy_moliya` INT NULL,
  `mukam_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_minor_mukam` (`is_id` ASC),
  CONSTRAINT `fk_isuv_minor_mukam`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_minor_mukam_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `minor_mukam_id` INT NULL,
  `mukam_miqdor_amalda` INT NULL,
  `mukam_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_minor_mukam_amalda` (`minor_mukam_id` ASC),
  CONSTRAINT `fk_isuv_minor_mukam_amalda`
    FOREIGN KEY (`minor_mukam_id`)
    REFERENCES `isuv_minor_mukam` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_nasos_yangi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `yangi_miqdor` INT NULL,
  `yangi_qiy` DECIMAL NULL,
  `yangi_qiy_moliya` INT NULL,
  `yangi_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_nasos_yangi` (`is_id` ASC),
  CONSTRAINT `fk_isuv_nasos_yangi`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_nasos_yangi_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nasos_yangi_id` INT NULL,
  `yangi_miqodr_amalda` INT NULL,
  `yangi_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_nasos_yangi_amalda` (`nasos_yangi_id` ASC),
  CONSTRAINT `fk_isuv_nasos_yangi_amalda`
    FOREIGN KEY (`nasos_yangi_id`)
    REFERENCES `isuv_nasos_yangi` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_nasos_rek` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `rek_miqdor` INT NULL,
  `rek_qiy` DECIMAL NULL,
  `rek_qiy_moliya` INT NULL,
  `rek_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_nasos_rek` (`is_id` ASC),
  CONSTRAINT `fk_isuv_nasos_rek`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_nasos_rek_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nasos_rek_id` INT NULL,
  `rek_miqdor_amalda` INT NULL,
  `rek_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_nasos_rek_amalda` (`nasos_rek_id` ASC),
  CONSTRAINT `fk_isuv_nasos_rek_amalda`
    FOREIGN KEY (`nasos_rek_id`)
    REFERENCES `isuv_nasos_rek` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_nasos_mukam` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `mukam_miqdor` DECIMAL NULL,
  `mukam_qiy` DECIMAL NULL,
  `mukam_qiy_moliya` INT NULL,
  `mukam_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_nasos_mukam` (`is_id` ASC),
  CONSTRAINT `fk_isuv_nasos_mukam`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_nasos_mukam_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nasos_mukam_id` INT NULL,
  `mukam_miqdor_amalda` INT NULL,
  `mukam_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_nasos_mukam_amalda` (`nasos_mukam_id` ASC),
  CONSTRAINT `fk_isuv_nasos_mukam_amalda`
    FOREIGN KEY (`nasos_mukam_id`)
    REFERENCES `isuv_nasos_mukam` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_xlor_yangi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `is_id` INT NULL,
  `yangi_miqdor` INT NULL,
  `yangi_qiy` DECIMAL NULL,
  `yangi_qiy_moliya` INT NULL,
  `yangi_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_xlor_yangi` (`is_id` ASC),
  CONSTRAINT `fk_isuv_xlor_yangi`
    FOREIGN KEY (`is_id`)
    REFERENCES `ichimliksuv` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `isuv_xlor_yangi_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `xlor_yangi_id` INT NULL,
  `yangi_miqdor_amalda` INT NULL,
  `yangi_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `isuv_xlor_yangi_amalda` (`xlor_yangi_id` ASC),
  CONSTRAINT `fk_isuv_xlor_yangi_amalda`
    FOREIGN KEY (`xlor_yangi_id`)
    REFERENCES `isuv_xlor_yangi` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `xandaq` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `tozalash_reja` INT NULL,
  `tozalash_qiy` DECIMAL NULL,
  `tozalash_qiy_moliya` INT NULL,
  `tozalash_qiy_molled` DECIMAL NULL,
  `oqava_km_reja` DECIMAL NULL,
  `oqava_qiy_reja` DECIMAL NULL,
  `oqava_qiy_moliya` INT NULL,
  `oqava_qiy_molled` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `xandaq_plan` (`plan_id` ASC),
  CONSTRAINT `fk_xandaq_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида тозалаш иншоотлари ва оқава сув хандаклари борасида амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `xandaq_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `xandaq_id` INT NULL,
  `tozalash_fakt` INT NULL,
  `tozalash_qiy_used` DECIMAL NULL,
  `oqava_fakt` DECIMAL NULL,
  `oqava_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `xandaq_fakt` (`xandaq_id` ASC),
  CONSTRAINT `fk_xandaq_fakt`
    FOREIGN KEY (`xandaq_id`)
    REFERENCES `xandaq` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `avtoyul` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `avtoyul_plan` (`plan_id` ASC),
  CONSTRAINT `fk_avtoyul_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида автомобиль йўлларини таъмирлаш борасида амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `ay_markaz_kap` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `avtoyul_id` INT NULL,
  `kap_km` DECIMAL NULL,
  `kap_qiy` DECIMAL NULL,
  `kap_qiy_moliya` INT NULL,
  `kap_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `ay_markaz_kap` (`avtoyul_id` ASC),
  CONSTRAINT `fk_ay_markaz_kap`
    FOREIGN KEY (`avtoyul_id`)
    REFERENCES `avtoyul` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ay_markaz_kap_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `markaz_kap_id` INT NULL,
  `kap_km_amalda` DECIMAL NULL,
  `kap_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `ay_markaz_kap_amalda` (`markaz_kap_id` ASC),
  CONSTRAINT `fk_ay_markaz_kap_amalda`
    FOREIGN KEY (`markaz_kap_id`)
    REFERENCES `ay_markaz_kap` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ay_markaz_joriy` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `avtoyul_id` INT NULL,
  `joriy_km` DECIMAL NULL,
  `joriy_qiy` DECIMAL NULL,
  `joriy_qiy_moliya` INT NULL,
  `joriy_qiy_molled` DECIMAL NULL,
  `joriy_asfal_km` DECIMAL NULL,
  `joriy_asfal_qiy` DECIMAL NULL,
  `joriy_asfal_qiy_moliya` INT NULL,
  `joriy_asfal_qiy_molled` DECIMAL NULL,
  `joriy_shagal_km` DECIMAL NULL,
  `joriy_shagal_qiy` DECIMAL NULL,
  `joriy_shagal_qiy_moliya` INT NULL,
  `joriy_shagal_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `ay_markaz_joriy` (`avtoyul_id` ASC),
  CONSTRAINT `fk_ay_markaz_joriy`
    FOREIGN KEY (`avtoyul_id`)
    REFERENCES `avtoyul` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ay_markaz_joriy_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `markaz_joriy_id` INT NULL,
  `joriy_km_amalda` DECIMAL NULL,
  `joriy_qiy_used` DECIMAL NULL,
  `joriy_asfal_km_amalda` DECIMAL NULL,
  `joriy_asfal_qiy_used` DECIMAL NULL,
  `joriy_shagal_km_amalda` DECIMAL NULL,
  `joriy_shagal_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `ay_markaz_joriy_amalda` (`markaz_joriy_id` ASC),
  CONSTRAINT `fk_ay_markaz_joriy_amalda`
    FOREIGN KEY (`markaz_joriy_id`)
    REFERENCES `ay_markaz_joriy` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ay_ichki` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `avtoyul_id` INT NULL,
  `kap_km` DECIMAL NULL,
  `kap_qiy` DECIMAL NULL,
  `kap_qiy_moliya` INT NULL,
  `kap_qiy_molled` DECIMAL NULL,
  `joriy_km` DECIMAL NULL,
  `joriy_qiy` DECIMAL NULL,
  `joriy_qiy_moliya` INT NULL,
  `joriy_qiy_molled` DECIMAL NULL,
  `joriy_asfal_km` DECIMAL NULL,
  `joriy_asfal_qiy` DECIMAL NULL,
  `joriy_asfal_qiy_moliya` INT NULL,
  `joriy_asfal_qiy_molled` DECIMAL NULL,
  `joriy_shagal_km` DECIMAL NULL,
  `joriy_shagal_qiy` DECIMAL NULL,
  `joriy_shagal_qiy_moliya` INT NULL,
  `joriy_shagal_qiy_molled` DECIMAL NULL,
  PRIMARY KEY (`id`),
  INDEX `ay_ichki` (`avtoyul_id` ASC),
  CONSTRAINT `fk_ay_ichki`
    FOREIGN KEY (`avtoyul_id`)
    REFERENCES `avtoyul` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ay_ichki_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ichki_id` INT NULL,
  `joriy_km_amalda` DECIMAL NULL,
  `joriy_qiy_used` DECIMAL NULL,
  `joriy_asfal_km_amalda` DECIMAL NULL,
  `joriy_asfal_qiy_used` DECIMAL NULL,
  `joriy_shagal_km_amalda` DECIMAL NULL,
  `joriy_shagal_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `ay_ichki_amalda` (`ichki_id` ASC),
  CONSTRAINT `fk_ay_ichki_amalda`
    FOREIGN KEY (`ichki_id`)
    REFERENCES `ay_ichki` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `avtobus` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NOT NULL,
  `xarid_miqdor` INT NULL,
  `xarid_qiy_uz` DECIMAL NULL,
  `xarid_qiy_bank` DECIMAL NULL,
  `bekat_yang_miqdor` INT NULL,
  `bekat_yangi_qiy` DECIMAL NULL,
  `bekat_yangi_qiy_molled` DECIMAL NULL,
  `bekat_rek_miqdor` INT NULL,
  `bekat_rek_qiy` DECIMAL NULL,
  `bekat_rek_qiy_molled` DECIMAL NULL,
  `yunal_yangi_miqdor` INT NULL,
  `yunal_change_miqdor` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `avtobus_plan` (`plan_id` ASC),
  CONSTRAINT `fk_avtobus_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида аҳолига транспорт хизмати кўрсатиш тизимини такомиллаштириш борасида амалга оширилаётган ишлар тўғрисида';

CREATE TABLE IF NOT EXISTS `avtobus_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `avtobus_id` INT NULL,
  `xarid_miqdor_amalda` INT NULL,
  `xarid_qiy_uz_amalda` DECIMAL NULL,
  `xarid_qiy_bank_amalda` DECIMAL NULL,
  `bekat_yangi_miqdor_amalda` INT NULL,
  `bekat_yangi_qiy_used` DECIMAL NULL,
  `bekat_rek_miqdor_amalda` INT NULL,
  `bekat_rek_qiy_used` DECIMAL NULL,
  `yunal_yangi_miqdor_amalda` INT NULL,
  `yunal_change_miqdor_amalda` INT NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `avtobus_amalda` (`avtobus_id` ASC),
  CONSTRAINT `fk_avtobus_amalda`
    FOREIGN KEY (`avtobus_id`)
    REFERENCES `avtobus` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `elektr_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `elektr_id` INT NOT NULL,
  `uyangi_dona_amalda` INT NULL,
  `uyangi_used` DECIMAL NULL,
  `ualma_dona_fakt` INT NULL,
  `ualma_qiy_used` DECIMAL NULL,
  `urek_dona_amalda` INT NULL,
  `urek_qiy_used` DECIMAL NULL,
  `eyangi_dona_amalda` INT NULL,
  `eyangi_qiy_used` DECIMAL NULL,
  `ealm_dona_amalda` INT NULL,
  `ealm_qiy_used` DECIMAL NULL,
  `erek_dona_amalda` INT NULL,
  `erek_qiy_used` DECIMAL NULL,
  `tyangi_dona_amalda` INT NULL,
  `tyangi_qiy_used` DECIMAL NULL,
  `talm_dona_amalda` INT NULL,
  `talm_qiy_used` DECIMAL NULL,
  `trek_dona_amalda` INT NULL,
  `trek_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `elektr_fakt` (`elektr_id` ASC),
  CONSTRAINT `fk_elektr_fakt`
    FOREIGN KEY (`elektr_id`)
    REFERENCES `elektr` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `energiya_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `energiya_id` INT NOT NULL,
  `elektr_km_fakt` DECIMAL NULL,
  `elektr_qiy_fakt` DECIMAL NULL,
  `elektr_stan_fakt` DECIMAL NULL,
  `tgaz_km_fakt` DECIMAL NULL,
  `tgaz_qiy_fakt` DECIMAL NULL,
  `sgaz_son_fakt` DECIMAL NULL,
  `sgaz_qiy_fakt` DECIMAL NULL,
  `kumir_hajm_fakt` DECIMAL NULL,
  `kumir_qiy_fakt` DECIMAL NULL,
  `suv_km_fakt` DECIMAL NULL,
  `suv_qiy_fakt` DECIMAL NULL,
  `toza_son_fakt` DECIMAL NULL,
  `toza_qiy_fakt` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `energiya_fakt` (`energiya_id` ASC),
  CONSTRAINT `fk_energiya_fakt`
    FOREIGN KEY (`energiya_id`)
    REFERENCES `energiya` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `gaz_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gaz_id` INT NOT NULL,
  `qyot_km_fakt` DECIMAL NULL,
  `qyot_qiy_used` DECIMAL NULL,
  `qtam_km_fakt` DECIMAL NULL,
  `qtam_qiy_used` DECIMAL NULL,
  `qdem_km_fakt` DECIMAL NULL,
  `qdem_qiy_used` DECIMAL NULL,
  `pyangi_dona_fakt` INT NULL,
  `pyangi_qiy_used` DECIMAL NULL,
  `prek_dona_fakt` INT NULL,
  `prek_qiy_used` DECIMAL NULL,
  `balon_dona_fakt` INT NULL,
  `balon_qiy_used` DECIMAL NULL,
  `kumir_tn_fakt` DECIMAL NULL,
  `kumir_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `gaz_fakt` (`gaz_id` ASC),
  CONSTRAINT `fk_gaz_fakt`
    FOREIGN KEY (`gaz_id`)
    REFERENCES `gaz` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ijtimoiy_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ijtimoiy_id` INT NOT NULL,
  `ijtimoi_son_fakt` INT NULL,
  `ijtimoi_qiy_fakt` DECIMAL NULL,
  `maktab_son_fakt` INT NULL,
  `maktab_qiy_fakt` DECIMAL NULL,
  `boqcha_son_fakt` INT NULL,
  `boqcha_qiy_fakt` DECIMAL NULL,
  `tib_son_fakt` INT NULL,
  `tib_qiy_fakt` DECIMAL NULL,
  `mfy_son_fakt` INT NULL,
  `mfy_qiy_fakt` DECIMAL NULL,
  `boshqa_son_fakt` INT NULL,
  `boshqa_qiy_fakt` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `ijtimoiy_fakt` (`ijtimoiy_id` ASC),
  CONSTRAINT `fk_ijtimoiy_fakt`
    FOREIGN KEY (`ijtimoiy_id`)
    REFERENCES `ijtimoiy` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yakkauy_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yakkauy_id` INT NOT NULL,
  `tom_son_fakt` INT NULL,
  `tom_kvm_fakt` DECIMAL NULL,
  `tom_qiy_used` DECIMAL NULL,
  `fasad_son_fakt` INT NULL,
  `fasad_kvm_fakt` DECIMAL NULL,
  `fasad_qiy_used` DECIMAL NULL,
  `darvoza_son_fakt` INT NULL,
  `darvoza_qiy_used` DECIMAL NULL,
  `hojat_son_fakt` INT NULL,
  `hojat_qiy_used` DECIMAL NULL,
  `tusiq_miq_fakt` DECIMAL NULL,
  `tusiq_qiy_used` DECIMAL NULL,
  `tusiq_tamir_miq_fakt` DECIMAL NULL,
  `tusiq_tamir_qiy_used` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yakkauy_fakt` (`yakkauy_id` ASC),
  CONSTRAINT `fk_yakkauy_fakt`
    FOREIGN KEY (`yakkauy_id`)
    REFERENCES `yakkauy` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `qishobod_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `qishobod_id` INT NOT NULL,
  `tratuar_km_fakt` DECIMAL NULL,
  `tratuar_qiy_fakt` DECIMAL NULL,
  `diod_dona_fakt` INT NULL,
  `diod_qiy_fakt` DECIMAL NULL,
  `gulzor_km_fakt` DECIMAL NULL,
  `gulzor_qiy_fakt` DECIMAL NULL,
  `boshqa_dona_fakt` DECIMAL NULL,
  `boshqa_qiy_fakt` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `qishobod_fakt` (`qishobod_id` ASC),
  CONSTRAINT `fk_qishobod_fakt`
    FOREIGN KEY (`qishobod_id`)
    REFERENCES `qishobod` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `qurmaterial_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `qurmaterial_id` INT NOT NULL,
  `metal_fakt` DECIMAL NULL,
  `cment_fakt` DECIMAL NULL,
  `shifer_fakt` INT NULL,
  `gisht_fakt` INT NULL,
  `shagal_fakt` DECIMAL NULL,
  `qum_fakt` DECIMAL NULL,
  `yogoc_fakt` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `qurmat_fakt` (`qurmaterial_id` ASC),
  CONSTRAINT `fk_qurmat_fakt`
    FOREIGN KEY (`qurmaterial_id`)
    REFERENCES `qurmaterial` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `yul_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `yul_id` INT NOT NULL,
  `avto_km_fakt` DECIMAL NULL,
  `avto_qiy_fakt` DECIMAL NULL,
  `bus_dona_fakt` INT NULL,
  `bus_qiy_fakt` DECIMAL NULL,
  `bekat_dona_fakt` INT NULL,
  `bekat_qiy_fakt` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `yul_fakt` (`yul_id` ASC),
  CONSTRAINT `fk_yul_fakt`
    FOREIGN KEY (`yul_id`)
    REFERENCES `yul` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `bozproject_fakt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bozproject_id` INT NOT NULL,
  `uzmab_fakt` DECIMAL NULL,
  `kredit_fakt` DECIMAL NULL,
  `boshqa_fakt` DECIMAL NULL,
  `ish_fakt` INT NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `bozproject_fakt` (`bozproject_id` ASC),
  CONSTRAINT `fk_bozproject_fakt`
    FOREIGN KEY (`bozproject_id`)
    REFERENCES `bozproject` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `qurulish` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `plan_id` INT NULL,
  `type` INT NULL COMMENT 'maktab\nmaktabgacha talim\nkasalxona\nmalla obektlari\nboshqa ijtimoiy obektlar',
  `title` VARCHAR(255) NULL,
  `urni` INT NULL,
  `baj_ish_turi` INT NULL,
  `qiy_reja` DECIMAL NULL,
  `qiy_moliya` INT NULL,
  `qiy_molled` DECIMAL NULL,
  `ishga_tushish_reja` DATE NULL,
  `asos_akt` VARCHAR(255) NULL,
  `asos_date` DATE NULL,
  `loyiha_smeta_hujjat` INT NULL COMMENT 'Лойиҳа смета ҳужжатининг мавжудлиги\n(ишлаб чиқилган, ишлаб чиқилмаган)',
  `tarmoq_jadvali` INT NULL COMMENT 'Тармоқ жадвалининг мавжудлиги\n(тасдиқланган, ишлаб чиқилмаган)',
  `loyihachi_tashkilot` VARCHAR(255) NULL,
  `pudratchi_tashkilot` VARCHAR(255) NULL,
  `maktab_raqam` VARCHAR(45) NULL COMMENT 'Мактаб рақами \n(Х-сонли мактаб)',
  `mtm_mulkchilik_shakli` INT NULL COMMENT 'Мулкчилик шакли\n(Давлат,\nДХШ,\nХусусий)',
  `mtm_raqam` VARCHAR(45) NULL COMMENT 'МТМ рақами \n(Х-сонли МТМ)',
  `ssm` VARCHAR(255) NULL COMMENT 'Соғлиқни сақлаш муассасаси \n(ҚВП, ҚОП,\nкасалхона) номи',
  PRIMARY KEY (`id`),
  INDEX `qurulish_plan` (`plan_id` ASC),
  CONSTRAINT `gk_qurulish_plan`
    FOREIGN KEY (`plan_id`)
    REFERENCES `locality_plans` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
COMMENT = 'Дастур доирасида умумий ўрта таълим мактабларини қуриш ва таъмирлаш борасида амалга оширилаётган ишлар тўғрисида\nmaktab,maktabgacha talim,kasalxona,malla obektlari,boshqa ijtimoiy obektlar';

CREATE TABLE IF NOT EXISTS `qurulish_amalda` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `qurulish_id` INT NULL,
  `qiy_used` DECIMAL NULL,
  `oh_yer` DECIMAL NULL,
  `oh_poydevor` DECIMAL NULL,
  `oh_gisht_terish` DECIMAL NULL,
  `oh_tom_yopish` DECIMAL NULL,
  `oh_demontaj` DECIMAL NULL,
  `oh_oraliq_devor` DECIMAL NULL,
  `oh_eshik_deraza` DECIMAL NULL,
  `oh_ichki_pardozlash` DECIMAL NULL,
  `oh_tashqi_pardozlash` DECIMAL NULL,
  `date` DATE NULL,
  `created_at` INT NULL,
  `updated_at` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `qurulish_amalda` (`qurulish_id` ASC),
  CONSTRAINT `fk_qurulish_amalda`
    FOREIGN KEY (`qurulish_id`)
    REFERENCES `qurulish` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190411_052332_last_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190411_052332_last_tables cannot be reverted.\n";

        return false;
    }
    */
}
