$(document).ready(function () {
    (function($){
        $(document).on('click', '.showModalButton', function () {
            if ($('#modal').data('bs.modal').isShown) {
                $('#modal').find('#modalContent')
                    .load($(this).attr('href'));
            } else {
                //if modal isn't open; open it and load content
                $('#modal').modal('show')
                    .find('#modalContent')
                    .load($(this).attr('href'));
            }
        });
    })(jQuery);
})
