<?php

namespace app\controllers;

use app\models\Aholi;
use Yii;
use app\models\LocalityPlans;
use app\models\search\LocalityPlansSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * AholiController implements the CRUD actions for LocalityPlans model.
 */
class AholiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LocalityPlans models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LocalityPlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all LocalityPlans models.
     * @return mixed
     */
    public function actionFilled()
    {
        $searchModel = new LocalityPlansSearch();
        $dataProvider = $searchModel->filled();

        return $this->render('filled', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the LocalityPlans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LocalityPlans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LocalityPlans::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionFill($plan_id)
    {
        $model = new Aholi(['scenario' => 'add']);
        $model->plan_id = $plan_id;
        $model->user_id = Yii::$app->user->getId();

        //Check auth&access for adding
        $plan = LocalityPlans::findOne($plan_id);
        if ($plan->locality->district->region_id != Yii::$app->access->getRegionId()) return 'error auth!';
        if (Aholi::find()->where(['plan_id' => $plan_id])->exists()) return 'error access!';

        //validate & add
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return Json::encode(array('status' => 'success', 'type' => 'success', 'message' => 'Contact created successfully.'));
                //return $this->goBack('/aholi/index');
            } else {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

        }

        return $this->renderAjax('_add_aholi', [
            'model' => $model,
        ]);

    }

    public function actionFillValidate($plan_id)
    {
        $model = new Aholi(['scenario' => 'add']);
        $model->plan_id = $plan_id;
        $model->user_id = Yii::$app->user->getId();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
}
