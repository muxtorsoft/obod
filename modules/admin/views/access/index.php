<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserAccessSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Accesses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-access-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User Access'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    \app\models\User::findOne($model->user_id)->username;
                }
            ],
            [
                'label' => 'Вилоят',
                'value' => function ($model) {
                    if(isset($model->region_id)){
                        return \app\models\Region::findOne($model->region_id)->title;
                    }else{
                        return null;
                    }
                }
            ],
            'type',
            //'to_id',
            //'create',
            //'update',
            //'view',
            //'delete',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
