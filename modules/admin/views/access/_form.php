<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-access-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(),'id','title'),['prompt'=>'Худудни танланг']) ?>

    <?php
    $sub = [];
    if($model->isNewRecord) $sub = \app\models\UserAccess::find()->select('user_id');
    $users = \app\models\Users::find()->where(['not in','id',$sub])->all();
    ?>
    <?= $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map($users,'id','username'),['prompt'=>'Фойдаланувчини танланг']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Get access'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
