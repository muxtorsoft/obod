<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\LocalityPlans */
/* @var $form yii\widgets\ActiveForm */

//Получить список уже имющие насленние пункты
$sub = \app\models\LocalityPlans::find()
    ->select('locality_id')
    ->where(['year' => $model->year])
    ->groupBy('locality_id')
    ->asArray();

//Получить список насленние пункты который нет в планы на текушие год
$locality = \app\models\Locality::find()->select(['id', 'title']);
if (!empty($model->district_id)) {
    $locality->where(['district_id' => $model->district_id]);
} else {
    $locality->where(['not in', 'id', $sub]);
}
$locality = $locality->all();

//Получить регионы
$region = \app\models\Region::find()->select(['id', 'title'])->all();

//Получить районы
$district = \app\models\District::find()->select(['id', 'title']);
if (!empty($model->region_id)) $district->where(['region_id' => $model->region_id]);
$district = $district->all();
?>

<div class="locality-plans-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($region, 'id', 'title'),
        [
            'prompt' => 'Худудни танланг',
            'onchange' => '
                $.get( "' . Url::to('districts') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'district_id') . '" ).html( data );                        
                    }
                );
                $.get( "' . Url::to('locals') . '", { id: null, region_id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'locality_id') . '" ).html( data );
                    }
                );
            '
        ]
    ) ?>

    <?= $form->field($model, 'district_id')->dropDownList(ArrayHelper::map($district, 'id', 'title'),
        [
            'prompt' => 'Туман ёки шахарни танланг',
            'onchange' => '
                $.get( "' . Url::to('locals') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'locality_id') . '" ).html( data );
                    }
                );
            '
        ]
    ) ?>

    <?= $form->field($model, 'locality_id')->dropDownList(ArrayHelper::map($locality, 'id', 'title'), ['prompt' => 'Кишлок ёки махаллани танланг']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
