<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Aholi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aholi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'plan_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\LocalityPlans::find()->all(), 'id', 'year'), ['prompt' => 'Кишлок ёки махаллани танланг']) ?>

    <?= $form->field($model, 'aholi_soni')->textInput() ?>

    <?= $form->field($model, 'xonadon_soni')->textInput() ?>

    <?= $form->field($model, 'oila_soni')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
