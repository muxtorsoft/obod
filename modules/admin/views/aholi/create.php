<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Aholi */

$this->title = Yii::t('app', 'Create Aholi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Aholis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aholi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
