<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\AholiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aholi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'locality_id') ?>

    <?= $form->field($model, 'plan_id') ?>

    <?= $form->field($model, 'aholi_soni') ?>

    <?= $form->field($model, 'xonadon_soni') ?>

    <?php // echo $form->field($model, 'oila_soni') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
