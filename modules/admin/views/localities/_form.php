<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Locality */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="locality-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group field-locality-district_id has-success">
        <label class="control-label" for="locality-district_id">Region ID</label>
        <?= Html::dropDownList('a',null,\yii\helpers\ArrayHelper::map(\app\models\Region::find()->all(),'id','title'),
            [
                'class' => 'form-control',
                'prompt' => Yii::t('app', 'Select region'),
                'onchange' => '
                $.get( "' . \yii\helpers\Url::to('districts') . '", { id: $(this).val() } )
                    .done(function( data ) {
                        $( "#' . Html::getInputId($model, 'district_id') . '" ).html( data );                        
                    }
                );'
            ]) ?>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'district_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\District::find()->all(),'id','title')) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'qishloq' => 'Qishloq', 'mahalla' => 'Mahalla' ], ['prompt' => '']) ?>

    <?= $form->field($model, 'sector_reg')->dropDownList([ '1' => '1', '2' => '2', '3' => '3', '4' => '4' ], ['prompt' => '']) ?>

    <?= $form->field($model, 'sector_dis')->dropDownList([ '1' => '1', '2' => '2', '3' => '3', '4' => '4' ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
