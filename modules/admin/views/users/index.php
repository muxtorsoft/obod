<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Users'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            [
                'label' => 'Вилоят',
                'value' => function ($model) {
                    $access = \app\models\UserAccess::find()->where(['user_id'=>$model->id])->one();
                    if(isset($access->region_id)){
                        return \app\models\Region::findOne($access->region_id)->title;
                    }else{
                        return null;
                    }
                }
            ],
            'userInfo.fio',
            //'userInfo.buyruq',
            'userInfo.telefon',
            //'userInfo.passport',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            //'email:email',
            'role',
            'status',
            [
                'label' => 'ФИО',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Yii::t('app', 'Киритиш!'),
                        ['info', 'user_id' => $model->id],
                        [
                            'title' => Yii::t('app', 'Ходим хакида маълумот'),
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'class' => 'showModalButton btn btn-danger'
                        ]
                    );
                }
            ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<div class="modal remote fade" id="modal">
    <div class="modal-dialog">
        <div id="modalContent" class="modal-content loader-lg"></div>
    </div>
</div>
