<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\RegionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Regions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Импорт'), ['import'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'title',
            [
                'label'=>'Ходимлар',
                'format'=>'raw',
                'value'=>function($model){
                    $c = '';
                    $e = $model->employee;
                    if(!empty($e)){
                        foreach ($e as $key=>$item){
                            $c .=  $item.' '.Html::a('Узгартириш!',['access/update','id'=>$key],['class' => 'btn btn-default',]);
                            $c .=  ' '.Html::a('Учириш!',['access/delete','id'=>$key],
                                [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                ],
                            ]);
                        }
                    }else{
                        $c .= Html::a('Кушиш!',['access/create','region_id'=>$model->id],['class' => 'btn btn-success',]);
                    }
                    return $c;
                },
            ],
            'order',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
