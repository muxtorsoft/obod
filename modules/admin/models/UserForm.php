<?php

namespace app\modules\admin\models;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 * User form
 */
class UserForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $status;
    public $role;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            [
                'username',
                'unique',
                'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.',
                'on' => 'create'
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [   'email',
                'unique',
                'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.',
                'on' => 'create'
            ],

            ['password', 'required', 'on' => 'create'],
            ['password', 'string', 'min' => 6],

            ['status', 'integer'],
            ['status', 'safe'],
            ['role', 'safe'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->role = $this->role;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }

    /**
     * Update user.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function update($id)
    {
        if (!$this->validate()) {
            return null;
        }

        $user = User::findOne($id);

        if($user==null) return null;

        //$user->username = $this->username;
        //$user->email = $this->email;
        $user->status = $this->status;
        $user->role = $this->role;
        if(!empty($this->password)) $user->setPassword($this->password);
        //$user->generateAuthKey();

        return $user->update() ? $user : null;
    }
}
