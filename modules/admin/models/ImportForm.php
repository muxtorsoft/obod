<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ImportForm extends Model
{
    public $file;
    public $type;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string'],
            [['file'], 'file', 'skipOnEmpty' => false],
            [['file'], 'file', 'extensions' => ['csv', 'xls', 'xlsx']],
            [['file'], 'file', 'checkExtensionByMimeType' => false, 'extensions' => ['csv', 'xls', 'xlsx']],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Кишлок ва махаллар, таман ва вилоятларга бириктирилган холда',
            'type' => 'Шахар ёки туман?',
        ];
    }
}
