<?php

namespace app\modules\admin\controllers;

use app\models\District;
use Yii;
use app\models\LocalityPlans;
use app\modules\admin\models\LocalityPlansSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LocalityPlansController implements the CRUD actions for LocalityPlans model.
 */
class LocalityPlansController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LocalityPlans models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LocalityPlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LocalityPlans model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LocalityPlans model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LocalityPlans();
        $model->year = date('Y');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LocalityPlans model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LocalityPlans model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LocalityPlans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LocalityPlans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LocalityPlans::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     */
    public function actionDistricts($id){
        $rows = \app\models\District::find()->where(['region_id' => $id])->all();

        echo "<option value=''>Туман ёки шахарни танланг</option>";

        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->id'>$row->title</option>";
            }
        }

    }

    /**
     * @param $id
     */
    public function actionLocals($id=null,$region_id=null){
        $rows = \app\models\Locality::find();
        if($id!=null) {
            $rows->where(['district_id' => $id]);
        }elseif ($region_id!=null) {
            $sub = District::find()->select('id')->where(['region_id'=>$region_id]);
            $rows->where(['in','district_id', $sub]);
        }
        $rows = $rows->all();

        echo "<option value=''>Кишлок ёки махаллани танланг</option>";

        if(count($rows)>0){
            foreach($rows as $row){
                echo "<option value='$row->id'>$row->title</option>";
            }
        }

    }
}
